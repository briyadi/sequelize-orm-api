const express = require("express")
const router = express.Router()
const cors = require("cors")
const { Article, Category } = require("../models")

router.use(cors())
router.use(express.json())

// List all articles
router.get("/articles", async (req, res) => {
    let response = []
    try {
        response = await Article.findAll({
            include: [
                {
                    model: Category,
                    as: "category"
                }
            ]
        })
    } catch (e) {
        console.log(e.message)
    }

    res.json(response)
})

// Get single article by id
router.get("/articles/:id", async (req, res) => {
    let response = {}
    try {
        response = await Article.findOne({where: {id: req.params.id}})
    } catch (e) {
        console.log(e.message)
        response.error = e.message
        res.status(401)
    }

    res.json(response)
})

// Create new article
router.post("/articles", async (req, res) => {
    try {
        await Article.create({
            body: req.body.body,
            title: req.body.title,
            approved: req.body.approved === true,
            categoryId: req.body.categoryId,
            createdAt: new Date(),
            updatedAt: new Date(),
        })
    } catch (e) {
        console.log(e)
        console.log(e.message)
    }

    res.end()
})

// Update article by id
router.put("/articles/:id", async (req, res) => {
    try {
        await Article.update({
            body: req.body.body,
            title: req.body.title,
            categoryId: req.body.categoryId,
            approved: req.body.approved === true,
        }, {
            where: {id: req.params.id}
        })
    } catch (e) {
        console.log(e.message)
    }

    res.end()
})

// Remove article by id
router.delete("/articles/:id", async (req, res) => {
    try {
        await Article.destroy({
            where: {id: req.params.id}
        })
    } catch (e) {
        console.log(e.message)
    }

    res.end()
})

router.get("/categories", async (req, res) => {
    let response = []

    try {
        response = await Category.findAll()
    } catch (e) {
        console.log(e.message)
    }

    res.json(response)
})

module.exports = router