const { render } = require("ejs")
const express = require("express")
const app = express()
const article = require("./api/article")

app.set("view engine", "ejs")

app.use("/api/v1", article)

// List view
app.get("/", (req, res) => {
    res.render("articles/list")
})

// form create
app.get("/articles/form", (req, res) => {
    res.render("articles/form", { id: null })
})

// form edit
app.get("/articles/form/:id", (req, res) => {
    res.render("articles/form", { id: req.params.id })
})

// Detail view
app.get("/articles/:id", (req, res) => {
    res.render("articles/detail", {
        id: req.params.id
    })
})

app.listen(8000, () => console.log(`Server running on port 8000`))