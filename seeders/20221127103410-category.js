'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Categories', [
        {
            id: 1,
            name: 'JavaScript',
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        {
            id: 2,
            name: 'SQL',
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        {
            id: 3,
            name: 'ORM',
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        {
            id: 4,
            name: 'CSS',
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        {
            id: 5,
            name: 'HTML',
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Categories', {id: [1, 2, 3, 4, 5]}, {});
  }
};
