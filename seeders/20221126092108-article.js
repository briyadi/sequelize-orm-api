'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Articles', [{
      id: 1,
      title: "Sample 1",
      body: "Sample 1 body",
      approved: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: 2,
      title: "Sample 2",
      body: "Sample 2 body",
      approved: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Articles', {id: [1, 2]}, {});
  }
};
